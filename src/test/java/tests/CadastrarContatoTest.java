package tests;

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import suporte.Web;

@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "DadosContatoTest.csv")

public class CadastrarContatoTest {

    private WebDriver navegador;

    @Before
    public void setUp() {

        navegador = Web.createChrome();
    }

    @Test
    public void testIncluirContato(
            @Param(name = "nome") String nome,
            @Param(name = "email") String email,
            @Param(name = "comentario") String comentario) {

        new ClicarContato(navegador)
                .clicarMenu()
                .clicarFormulario()
                .informarNome(nome)
                .informarEmail(email)
                .informarComentario(comentario)
                .clicarEnviar();
    }

    @After
    public void tearDown() {

        navegador.quit();
    }
}
