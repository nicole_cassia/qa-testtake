package tests;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import suporte.Generator;
import suporte.Screenshot;


public class ClicarContato extends BasePage {

    String caminhoarquivo = "/home/nicole/Nicole/Projeto Automacao/ScreenShot/";

    public ClicarContato(WebDriver navegador) {
        super(navegador);
    }

    public ClicarContato clicarMenu() {

        navegador.findElement(By.className("header-navigation")).click();
        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "clicarMenu.png");

        return this;
    }


    public ClicarContato clicarFormulario() {

        JavascriptExecutor jse = (JavascriptExecutor) navegador;
        jse.executeScript("scrollBy (0, 250);");
        navegador.findElement(By.id("menu-item-7")).click();
        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "clicarFormulario.png");

        return this;

    }


    public ClicarContato informarNome(String nome) {


        navegador.findElement(By.id("g2-name")).sendKeys(nome);
        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "informarNome.png");

        return this;
    }

    public ClicarContato informarEmail(String email) {

        navegador.findElement(By.id("g2-email")).sendKeys(email);
        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "informarEmail.png");

        return this;
    }

    public ClicarContato informarComentario(String comentario) {

        navegador.findElement(By.id("contact-form-comment-g2-comment")).sendKeys(comentario);
        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "informarComentario.png");

        return this;
    }

    public ClicarContato clicarEnviar() {

        JavascriptExecutor jse = (JavascriptExecutor) navegador;
        jse.executeScript("scrollBy(0,250)", "");

       // navegador.findElement(By.cssSelector("p[class='contact-submit']")).click();

        navegador.findElement(By.cssSelector("button[type='submit'][class='pushbutton-wide']")).click();

        Screenshot.tirar(navegador, caminhoarquivo + Generator.DataHoraArquivo() + "clicarEnviar.png");

        WebElement mensagem = navegador.findElement(By.cssSelector("h3"));
        String confirmacao = mensagem.getText();
        Assert.assertEquals("A mensagem foi enviada (voltar)", confirmacao);

        return this;


    }


}




