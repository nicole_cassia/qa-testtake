package suporte;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class Web {

    public static WebDriver createChrome() {
        // Abrindo o Navegador
        System.setProperty("webdriver.chrome.driver", "/home/nicole/Nicole/Projeto Automacao/chromedriver_linux64/chromedriver");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Navegando para a página da Receita de Brownie de Chocolate!
        navegador.get("https://qadatake.wordpress.com/2018/07/12/the-journey-begins/");
        return navegador;

    }
}
